export const API = {
  KEY: "d6e5426d44456693697d3280bdd14b3b",
  URL: "https://api.themoviedb.org/3",
  MOVIE_SEARCH: "search/movie",
  POPULAR_MOVIES: "movie/popular",
  TOP_RATED_MOVIES: "movie/top_rated",
  UPCOMING_MOVIES: "movie/upcoming",
  MOVIE_DETAILS: "movie",
};
