export const HtmlMovieId = {
  FILM_CONTAINER: "film-container",
  FAVORITE_MOVIES: "favorite-movies",
  SEARCH: "search",
  POPULAR: "popular",
  UPCOMING: "upcoming",
  TOP_RATED: "top_rated"
};
