import { ExceptionName } from "../enums/exception-name.enum";
import { HttpCode } from "../enums/http-code.enum";

const DEFAULT_MESSAGE = "Network Error";

class HttpError extends Error {
  private status: number;

  constructor({
    status = HttpCode.INTERNAL_SERVER_ERROR,
    message = DEFAULT_MESSAGE
  } = {}) {
    super(message);
    this.status = status;
    this.name = ExceptionName.HTTP_ERROR;
  }
}

export { HttpError };
