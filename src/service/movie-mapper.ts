import { IncomingMovie } from "../data/incoming-movie-types";
import { MovieType } from "../data/movie-type";
import { MovieStorageService } from "./storage.service";

export function movieMapper(item: IncomingMovie): MovieType {
  const { id, poster_path, overview, release_date } = item;
  const isLiked = new MovieStorageService().getFavoriteMovieIds().includes(String(id));

  return {
    id,
    poster_path,
    overview,
    release_date,
    isLiked
  };
}
