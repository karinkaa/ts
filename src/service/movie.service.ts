import { IncomingMovie } from "../data/incoming-movie-types";
import { MovieType } from "../data/movie-type";
import { API } from "../enums/api.enum";
import { HttpMethod } from "../enums/http-method.enum";
import { Http } from "./http.service";
import { movieMapper } from "./movie-mapper";
import { MovieStorageService } from "./storage.service";

class MovieService {
  private apiPath: string;
  private http: Http;
  private storage: MovieStorageService;

  constructor({ apiPath, http, storage }: { apiPath: string, http: Http, storage: MovieStorageService }) {
    this.apiPath = apiPath;
    this.http = http;
    this.storage = storage;
  }

  async getAllMovies(filter: { query: string }): Promise<MovieType[]> {
    const { results } = await this.http.load(`${this.apiPath}/${API.MOVIE_SEARCH}`, {
      method: HttpMethod.GET,
      query: filter
    });

    return results.map(movieMapper);
  }

  async getPopularMovies(): Promise<MovieType[]> {
    const { results } = await this.http.load(`${this.apiPath}/${API.POPULAR_MOVIES}`, {
      method: HttpMethod.GET
    });

    return results.map(movieMapper);
  }

  async getUpcomingMovies(): Promise<MovieType[]> {
    const { results } = await this.http.load(`${this.apiPath}/${API.UPCOMING_MOVIES}`, {
      method: HttpMethod.GET
    });

    return results.map(movieMapper);
  }

  async getTopRatedMovies(): Promise<MovieType[]> {
    const { results } = await this.http.load(`${this.apiPath}/${API.TOP_RATED_MOVIES}`, {
      method: HttpMethod.GET
    });

    return results.map(movieMapper);
  }

  async getMovieDetails(id: string): Promise<MovieType> {
    const movie: IncomingMovie = await this.http.load(`${this.apiPath}/${API.MOVIE_DETAILS}/${id}`, {
      method: HttpMethod.GET
    });

    return movieMapper(movie);
  }

  async getFavoriteMovies(): Promise<MovieType[]> {
    const ids = this.storage.getFavoriteMovieIds();
    const movies = await Promise.all(ids.map(id => this.getMovieDetails(id)));
    return movies;
  }

  likeMovie(id: number): void {
    this.storage.toggleFavorite(id);
  }
}

export { MovieService };
