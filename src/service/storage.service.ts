class ObjectStorage {
  static get<T>(key: string): T | null {
    try {
      return JSON.parse(localStorage.getItem(key) || "");
    } catch (e) {
      return null;
    }
  }
  static set(key: string, value: any): void {
    localStorage.setItem(key, JSON.stringify(value));
  }
}

class MovieStorageService {
  private storageKey = "favoriteMovieIds";

  toggleFavorite(id: number): void {
    const values = ObjectStorage.get<string[]>(this.storageKey) || [];

    if (values?.includes(String(id))) {
      ObjectStorage.set(this.storageKey, values.filter(item => item !== String(id)));
    } else {
      ObjectStorage.set(this.storageKey, [...values, String(id)]);
    }
  }

  getFavoriteMovieIds(): string[] {
    return ObjectStorage.get(this.storageKey) || [];
  }
}

export { MovieStorageService };
