import { API } from "../enums/api.enum";
import { HttpMethod } from "../enums/http-method.enum";
import { HttpError } from "../http-error/http-error.exception";

class Http {
  load<T>(url: string, options: { method?: string, payload?: BodyInit, query?: Record<string, string> }): Promise<T> {
    const {
      method = HttpMethod.GET,
      payload = null,
      query
    } = options;

    return fetch(this.getUrl(url, query), {
      method,
      body: payload
    })
      .then(this.checkStatus)
      .then(this.parseJSON)
      .catch(this.throwError);
  }

  private async checkStatus(response: Response): Promise<Response> {
    if (!response.ok) {
      const parsedException = await response.json().catch(() => ({
        message: response.statusText
      }));

      throw new HttpError({
        status: response.status,
        message: parsedException?.message
      });
    }

    return response;
  }

  private getUrl(url: string, query?: Record<string, string>): string {
    return `${url}?api_key=${API.KEY}${query ? `&${new URLSearchParams(query).toString()}` : ''}`;
  }

  private parseJSON(response: Response): Promise<any> {
    return response.json();
  }

  private throwError(err: HttpError): HttpError {
    throw err;
  }
}

export { Http };
