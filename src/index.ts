import { MoviesView } from "./data/movies-view";
import { API } from "./enums/api.enum";
import { HtmlMovieId } from "./enums/html-movie-id.enums";
import { MovieRate } from "./enums/movie-rate.enum";
import { Http } from "./service/http.service";
import { MovieService } from "./service/movie.service";
import { MovieStorageService } from "./service/storage.service";

export async function render(): Promise<void> {
    const movieService = new MovieService({ apiPath: API.URL, http: new Http(), storage: new MovieStorageService() });

    const search = document.getElementById(HtmlMovieId.SEARCH);
    search?.addEventListener("change", async (e: any) => {
        const results = await movieService.getAllMovies({ query: e.target.value });
        new MoviesView(HtmlMovieId.FILM_CONTAINER).setMovies(results);
    });

    const popular = document.getElementById(HtmlMovieId.POPULAR);
    popular?.addEventListener("click", async () => {
        const results = await movieService.getPopularMovies();
        new MoviesView(HtmlMovieId.FILM_CONTAINER).setMovies(results);
    });

    const upcoming = document.getElementById(HtmlMovieId.UPCOMING);
    upcoming?.addEventListener("click", async () => {
        const results = await movieService.getUpcomingMovies();
        new MoviesView(HtmlMovieId.FILM_CONTAINER).setMovies(results);
    });

    const topRated = document.getElementById(HtmlMovieId.TOP_RATED);
    topRated?.addEventListener("click", async () => {
        const results = await movieService.getTopRatedMovies();
        new MoviesView(HtmlMovieId.FILM_CONTAINER).setMovies(results);
    });

    const favoriteButton = document.querySelector("[data-bs-target='#offcanvasRight']");
    favoriteButton?.addEventListener("click", async () => {
        const results = await movieService.getFavoriteMovies();
        new MoviesView(HtmlMovieId.FAVORITE_MOVIES).setMovies(results);
    });

    const likeMovie = document.getElementById(HtmlMovieId.FILM_CONTAINER);
    likeMovie?.addEventListener("click", async (e: any) => {
        const style = e.target?.parentNode?.style;
        style.fill = style.fill === MovieRate.LIKED ? MovieRate.UNLIKED : MovieRate.LIKED;

        const movieElement = e.target?.parentNode?.parentNode?.parentNode;
        const movieId = movieElement.dataset.movieId;

        if (movieId) {
            await movieService.likeMovie(movieId);
        }
    });
}
