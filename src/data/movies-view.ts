import { MovieType } from "./movie-type";
import { MovieView } from "./movie-view";

class MoviesView {
    private content: HTMLElement;
    private elementId: string;

    constructor(elementId: string) {
        this.content = (document.getElementById(elementId) as HTMLElement);
        this.elementId = elementId;
    }

    setMovies(movies: Array<MovieType>): void {
        this.content.innerHTML = "";
        movies.map((movie: MovieType) => this.content?.appendChild(new MovieView(movie, this.elementId).getView()));
    }

    getView(): HTMLElement {
        return this.content;
    }
}

export { MoviesView };
