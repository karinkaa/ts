export type MovieType = {
  id: number,
  poster_path: string,
  overview: string,
  release_date: string,
  isLiked: boolean
};
