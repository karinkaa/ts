import { HtmlMovieId } from "../enums/html-movie-id.enums";
import { MovieRate } from "../enums/movie-rate.enum";
import { MovieType } from "./movie-type";

const cardTemplate = `
<div class="#movie_card_style" data-movie-id=#movie_id>
    <div class="card shadow-sm">
        <img src="#poster_path">
        <svg xmlns="http://www.w3.org/2000/svg" stroke="red" fill="#fill_color" width="50" height="50" class="bi bi-heart-fill position-absolute p-2" viewBox="0 -2 18 22" data-darkreader-inline-fill="" data-darkreader-inline-stroke="" style="--darkreader-inline-fill:#cc0000; --darkreader-inline-stroke:#ff1a1a;">
            <path fill-rule="evenodd" d="M8 1.314C12.438-3.248 23.534 4.735 8 15-7.534 4.736 3.562-3.248 8 1.314z"></path>
        </svg>
        <div class="card-body">
            <p class="card-text truncate">
                #overview
            </p>
            <div class="
                    d-flex
                    justify-content-between
                    align-items-center
                ">
                <small class="text-muted">#release_date</small>
            </div>
        </div>
    </div>
</div>
`;

class MovieView {
    private content: HTMLElement;

    constructor({ id, poster_path, overview, release_date, isLiked }: MovieType, elementId: string) {
        const current = cardTemplate
            .replace("#movie_card_style", elementId === HtmlMovieId.FILM_CONTAINER ? "col-lg-3 col-md-4 col-12 p-2" : "col-12 p-2")
            .replace("#movie_id", String(id))
            .replace("#poster_path", 'https://image.tmdb.org/t/p/original/' + poster_path)
            .replace("#overview", overview)
            .replace("#release_date", release_date)
            .replace("#fill_color", isLiked ? MovieRate.LIKED : MovieRate.UNLIKED);

        this.content = this.createElementFromHTML(current);
    }

    private createElementFromHTML(htmlString: string): HTMLElement {
        const div = document.createElement('div');
        div.innerHTML = htmlString.trim();

        return (div.firstChild as HTMLElement);
    }

    getView(): HTMLElement {
        return this.content;
    }
}

export { MovieView };
